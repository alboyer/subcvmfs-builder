###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Steps module: defines the business logic of subcvmfs-builder.
"""

from concurrent.futures import ProcessPoolExecutor, as_completed
import json
import logging
from multiprocessing import Manager
import os
from pathlib import Path
import re
import shlex
import subprocess
import uuid

from pydantic import BaseModel


class CVMFSRepository(BaseModel):
    """CVMFS extension configuration"""

    # URL of the CVMFS repository
    url: str
    # Public key of the CVMFS repository
    public_key: str
    # List of paths to be mounted
    paths: list[Path] = []


def trace_ops(
    apps_dir: str,
    repositories: list[str],
    path_list: str,
    number_cores: int,
    container: str | None = None,
    out: str | None = None,
    err: str | None = None,
) -> bool:
    """Trace applications of interests and generate a list of paths involved in the process

    :param apps_dir: directory of applications structured as <apps_dir>/<command>/command.sh
    :param repositories: configuration of the repositories to use
    :param http_proxy: http proxy used to trace CVMFS
    :param container: name of the container in which applications will run
    :param path_list: file that will contain the list of the paths involved in the executions
    :param number_cores: number of operations to be done in parallel
    :return: True/False
    """
    namelist_content = set()

    manager = Manager()
    lock = manager.Lock()  # type: ignore

    failed_processes = 0
    with ProcessPoolExecutor(max_workers=number_cores) as executor:
        futures = [
            executor.submit(
                trace_a_command,
                input_command,
                repositories,
                lock,
                container,
                out,
                err,
            )
            for input_command in Path(apps_dir).glob("*/command.sh")
        ]
        for future in as_completed(futures):
            if not future.result():
                failed_processes += 1
            else:
                namelist_content.update(future.result())

    # Export content in path_list
    with open(path_list, "w") as f:
        for path_name in namelist_content:
            f.write(f"{path_name}\n")

    total_processes = len(futures)
    if failed_processes > 0:
        print(f"{failed_processes}/{total_processes} elements have not been processed")
        return False

    return True


def trace_a_command(
    input_command: Path,
    repositories: list[str],
    lock,
    container: str | None,
    out: str | None = None,
    err: str | None = None,
) -> list[str]:
    """Trace an application of interest (multiprocessing)

    :param input_command: directory containing a command and input dependencies
    :param repositories: configuration of the repositories to use
    :param http_proxy: http proxy used to trace CVMFS
    :param container: name of the container in which the application will run
    :param lock: lock to synchronize the access to the output files
    :param mountlist: list of path mounts involved in the tracing process
    """
    logging.info(f"Start tracing {input_command}")

    # Prepare trace command
    namelist = Path.cwd() / f"namelist_{uuid.uuid4()}.txt"
    namelist.unlink(missing_ok=True)
    command = generate_trace_command(namelist, input_command)

    # Generate the executable
    executable = Path.cwd() / f"exec_{uuid.uuid4()}.sh"
    generate_executable(executable, command)

    # Get the container if it exists
    container = get_specific_container(input_command, container)
    if not container:
        logging.error(f"No container found for {input_command}: cannot execute")
        return []

    logging.debug(f"Container found for {input_command}: will execute in {container}")
    # Set up apptainer command
    command = f"apptainer exec --cleanenv --bind {executable.parent.resolve()} --bind /cvmfs {container} {executable}"

    # Execute it
    logging.debug(f"Command: {command}")

    current_directory = Path.cwd()
    os.chdir(str(input_command.resolve().parent))

    result = subprocess.run(shlex.split(command), capture_output=bool(out or err))
    if result.returncode != 0:
        namelist.unlink(missing_ok=True)
        executable.unlink(missing_ok=True)
        os.chdir(str(current_directory))
        logging.error(f"{input_command} tracing failed")
        generate_command_outputs(
            result.stdout, result.stderr, out, err, lock, input_command
        )
        return []

    logging.info(f"{input_command} successfully traced")

    # Preprocess the result of the execution:
    # before:
    # <proc> <action>(<code>, <path> ...
    # after:
    # <path>
    namelist_content = preprocess_namelist(namelist, repositories)
    if not namelist_content:
        logging.warning(f"{input_command} successfully traced but no path found")
    else:
        logging.info(f"Matched with {len(namelist_content)} paths.")

    # Clean up
    executable.unlink()
    namelist.unlink()
    os.chdir(str(current_directory))
    generate_command_outputs(
        result.stdout, result.stderr, out, err, lock, input_command
    )
    return namelist_content


def generate_trace_command(namelist: Path, input_command: Path):
    """Generate an executable to execute an application

    :param file_name: executable file name
    :param app_path: path to the application to run
    """
    # `file` option is deprecated in newer versions of strace
    # Nevertheless this is needed to trace applications running in old systems
    modern_trace_prefix = f"strace -f -e trace=%file -o {str(namelist)} "
    deprecated_trace_prefix = f"strace -f -e trace=file -o {str(namelist)} "

    command = (
        f"#!/bin/bash\n"
        f"{modern_trace_prefix}{input_command.resolve()}\n"
        "if [ $? -ne 0 ]; then\n"
        f"\t{deprecated_trace_prefix}{input_command.resolve()}\n"
        "fi\n"
    )
    return command


def preprocess_namelist(namelist: Path, repositories: list[str]):
    """Preprocess the namelist content to extract and filter paths

    :param namelist: file containing a list of paths
    :param repositories: CVMFS repository details
    """
    # Escape any special regex characters in the repository paths and combine them into a single pattern
    combined_pattern = re.compile(
        "|".join(
            rf'.*?"({re.escape(repository)}[^"]+)".*?\)' for repository in repositories
        )
    )

    # Create a set to store unique file paths
    namelist_content = set()

    with open(namelist) as f:
        for line in f:
            match = combined_pattern.search(line)
            if match and match.lastindex:
                # Iterate over all groups and add the non-empty one to the set
                for i in range(1, match.lastindex + 1):
                    if match.group(i):
                        # Resolve the path but do not follow symlinks
                        normalized_path = Path(os.path.normpath(match.group(i)))
                        if normalized_path.exists():
                            namelist_content.add(normalized_path)
                        # Resolve the path and follow symlinks
                        resolved_path = Path(match.group(i)).resolve()
                        if resolved_path.exists():
                            namelist_content.add(resolved_path)

    return sorted(namelist_content)


# ------------------------------------------------------------------------------


def build_ops(
    path_list: list[str],
    repositories: dict[str, CVMFSRepository],
    subset_path: str,
    out: str | None = None,
    err: str | None = None,
) -> bool:
    """Build a subset of CVMFS based on a list of paths and a configuration

    :param path_list: file containing a list of paths
    :param repositories: CVMFS repository details
    :param subset_path: path to the subset of CVMFS
    """
    # Aggregate list of paths in namelist_paths
    namelist_paths = []
    for path_file in path_list:
        logging.debug(f"Collect {path_file} content")
        with open(path_file) as f:
            trace = f.read()
        namelist_paths.extend(re.findall("/cvmfs/.*", trace))

    # Remove non existing paths, resolve all paths
    process_user_paths(namelist_paths)
    real_paths = get_subset_paths(namelist_paths)

    # Sort paths according to the repository they come from
    process_subset_paths(repositories, real_paths)

    # Write paths in repository specification files
    for repository_name, repository in repositories.items():
        # Build the config and spec files
        spec, config = setup_config_spec_files(repository_name, repository)
        if not spec or not config:
            logging.warn(f"{repository_name} does not contain any path, we skip it")
            continue

        # Generate the subset of CVMFS
        command = f"cvmfs_shrinkwrap -r {repository_name} -f {config} -t {spec} --dest-base {subset_path}"
        logging.debug(f"Command: {command}")

        result = subprocess.run(shlex.split(command), capture_output=bool(out or err))
        if result.returncode != 0:
            Path(spec).unlink()
            Path(config).unlink()
            generate_command_outputs(result.stdout, result.stderr, out, err)
            return False

        Path(spec).unlink()
        Path(config).unlink()
        generate_command_outputs(result.stdout, result.stderr, out, err)
        logging.info(f"{subset_path} has been successfully created")

    return True


def process_user_paths(namelist_paths: list[str]):
    """Process paths added manually and ending with '*':

    :param namelist_paths: list of paths to potentially add to the subset
    """
    paths_to_remove = []
    paths_to_add = []

    for namelist_path in namelist_paths:
        trace_path = Path(namelist_path)

        if trace_path.name == "*":
            for subpath in trace_path.parent.rglob("*"):
                paths_to_add.append(f"{subpath}")
            paths_to_remove.append(namelist_path)

    for path_to_remove in paths_to_remove:
        namelist_paths.remove(path_to_remove)
    for path_to_add in paths_to_add:
        namelist_paths.append(path_to_add)


def get_subset_paths(namelist_paths: list[str]) -> set[Path]:
    """Get subset paths from a list of paths:
        - Remove non existing paths
        - Add symbolic link and real paths

    :param namelist_paths: list of paths to potentially add to the subset
    :return: list of paths
    """
    # Sort paths
    n_skipped = 0
    real_paths = set()

    for namelist_path in namelist_paths:
        trace_path = Path(namelist_path)

        # Check that path exists, else it is removed
        if not trace_path.exists():
            n_skipped += 1
            logging.debug(f"Skipped {trace_path}")
            continue

        # Check whether path is a symlink and add it if it's the case
        real_trace_path = trace_path.resolve()
        real_paths.add(real_trace_path)
        if real_trace_path != trace_path:
            real_paths.add(trace_path)

        # Check whether path is part of a symlink directory
        symlink_path = real_trace_path.parent.joinpath(trace_path.name)
        if trace_path != symlink_path and symlink_path.exists():
            real_paths.add(symlink_path)

    # Get total size
    total_size = 0
    for path in real_paths:
        total_size += path.stat().st_size

    logging.info(f"Skipped {n_skipped} paths that don't exist")
    logging.info(f"Found {len(real_paths)} paths that were opened")
    logging.info(f"Total size of files is {(total_size/1024**2)} MB")
    return real_paths


def process_subset_paths(repositories: dict[str, CVMFSRepository], paths: set[Path]):
    """Process path_list and store paths in repositories

    :param repositories: configuration parameters of repositories
    :param path_list: list of paths to process
    """
    for real_path in paths:
        repository_name = real_path.parts[2]
        if repository_name not in repositories:
            logging.debug(f"{real_path} was found but not integrated")
            continue

        path_to_add = Path("/").joinpath(
            real_path.relative_to(f"/cvmfs/{repository_name}")
        )
        repositories[repository_name].paths.append(path_to_add)


def setup_config_spec_files(
    repository_name: str, repository_content: CVMFSRepository
) -> tuple[str, str]:
    """Build CVMFS shrinkwrap config and spec files according to
        the repository info in parameters.

    :param repository_name: name of the repository
    :param repository_content: parameters related to a repository
    :return: tuple combining the name of the spec and the config files
    """
    if not repository_content.paths:
        logging.warn(f"{repository_name} does not contain any path, we skip it")
        return ("", "")

    spec = f"{repository_name}.spec"
    config = f"{repository_name}.config"

    # Build the spec file
    with open(spec, "w") as f:
        f.write("\n".join(list(map(str, repository_content.paths))))

    # Build the config file
    config_content = f"CVMFS_REPOSITORIES={repository_name}\n"
    config_content += f"CVMFS_REPOSITORY_NAME={repository_name}\n"
    config_content += f"CVMFS_SERVER_URL={repository_content.url}\n"
    config_content += (
        f"CVMFS_KEYS_DIR={str(Path(repository_content.public_key).parent)}\n"
    )
    config_content += "CVMFS_CONFIG_REPOSITORY=cvmfs-config.cern.ch\n"
    config_content += "CVMFS_HTTP_PROXY=DIRECT\n"
    config_content += "CVMFS_CACHE_BASE=/var/lib/cvmfs/shrinkwrap\n"
    config_content += "CVMFS_SHARED_CACHE=no\n"
    config_content += "CVMFS_USER=cvmfs"
    logging.debug(f"{config}:\n{config_content}")

    with open(config, "w") as f:
        f.write(config_content)

    return (spec, config)


# ------------------------------------------------------------------------------


def test_ops(
    apps_dir: str,
    subset_path: str,
    number_cores: int,
    container: str | None = None,
    out: str | None = None,
    err: str | None = None,
) -> bool:
    """Check the validity of the inputs

    :param apps_dir: directory of applications structured as <apps_dir>/<command>/command.sh
    :param subset_path: path of the subset of CVMFS
    :param container: name of the container in which applications will run
    :param number_cores: number of operations to be done in parallel
    """
    manager = Manager()
    lock = manager.Lock()  # type: ignore

    failed_processes = 0
    with ProcessPoolExecutor(max_workers=number_cores) as executor:
        futures = [
            executor.submit(
                test_a_command,
                input_command,
                subset_path,
                lock,
                container,
                out,
                err,
            )
            for input_command in Path(apps_dir).glob("*/command.sh")
        ]
        for future in as_completed(futures):
            if not future.result():
                failed_processes += 1

    total_processes = len(futures)
    if failed_processes > 0:
        print(f"{failed_processes}/{total_processes} elements have not been processed")
        return False

    return True


def test_a_command(
    input_command: Path,
    subset_path: str,
    lock,
    container: str | None = None,
    out: str | None = None,
    err: str | None = None,
) -> bool:
    """Test an application of interest (multiprocessing)

    :param input_command: directory containing a command and input dependencies
    :param trace_prefix: prefix command to trace the input command
    :param subset_path: path of the subset of CVMFS
    :param container: name of the container in which the application will run
    """
    logging.info(f"Start testing {input_command}")

    container = get_specific_container(input_command, container)
    subset_path_absolute = Path(subset_path).resolve()

    if not container:
        logging.error(f"No container found for {input_command}: cannot execute")
        return False

    # Generate the executable
    executable = Path.cwd().joinpath(f"exec_{uuid.uuid4()}.sh")
    generate_executable(executable, str(input_command.resolve()))

    # Set up apptainer command
    command = "apptainer exec --cleanenv "
    command += (
        f"--bind {executable.parent.resolve()} --bind {subset_path_absolute}:/cvmfs "
    )
    command += f"{container} {executable}"

    # Execute it
    logging.debug(f"Command: {command}")

    current_directory = Path.cwd()
    os.chdir(str(input_command.resolve().parent))

    result = subprocess.run(shlex.split(command), capture_output=bool(out or err))
    if result.returncode != 0:
        executable.unlink()
        os.chdir(str(current_directory))
        logging.error(f"{input_command} testing failed")
        generate_command_outputs(
            result.stdout, result.stderr, out, err, lock, input_command
        )
        return False

    # Clean up
    executable.unlink()
    os.chdir(str(current_directory))
    generate_command_outputs(
        result.stdout, result.stderr, out, err, lock, input_command
    )
    logging.info(f"{input_command} successfully tested")
    return True


# ------------------------------------------------------------------------------


def deploy_ops(
    subset_path: str,
    remote_location: str,
    out: str | None = None,
    err: str | None = None,
) -> bool:
    """Check the validity of the inputs

    :param subset_path: path of the subset of CVMFS
    :param remote_location: remote server address and path to destination
    """
    # Deploy to a remote server
    command = f"rsync -a {subset_path} {remote_location}"
    logging.debug(f"Command: {command}")

    result = subprocess.run(shlex.split(command), capture_output=bool(out or err))
    if result.returncode != 0:
        generate_command_outputs(result.stdout, result.stderr, out, err)
        return False

    generate_command_outputs(result.stdout, result.stderr, out, err)
    return True


# ------------------------------------------------------------------------------


def generate_executable(file_name: Path, command: str):
    """Generate an executable to execute an application

    :param file_name: executable file name
    :param app_path: path to the application to run
    """
    with open(file_name, "w") as f:
        f.write(f"#!/bin/bash\n{command}")

    file_path = Path(file_name)
    file_path.chmod(0o755)


def get_specific_container(
    input_command: Path, default_container: str | None
) -> str | None:
    """Get a specific container for a given command

    :param input_command: command path
    :param container: name of the default container in which applications will run
    """
    specific_container_details_path = input_command.parent / "container.json"
    if specific_container_details_path.exists():
        with open(specific_container_details_path) as f:
            specific_container_details = json.load(f)
        container = specific_container_details.get("container", default_container)
        logging.info(f"Found a specific container for {input_command}: {container}")
    else:
        container = default_container
    return container


def generate_command_outputs(
    stdout: bytes,
    stderr: bytes,
    out: str | None = None,
    err: str | None = None,
    lock=None,
    input_command: Path | None = None,
):
    """Generate a command output file containing details about an execution

    :param command_output: name of the files that will contain stdout and stderr
    :param stdout: stdout of a command
    :param stderr: stderr of a command
    :param input_command: directory containing a command and input dependencies
    """
    if lock:
        lock.acquire()
    try:
        header = ""
        if input_command:
            header = "==============================\n"
            header += f"{input_command}\n"
            header += "==============================\n"

        if out:
            with open(out, "a") as f:
                f.write(header)
                f.write(stdout.decode("utf-8", "backslashreplace"))
        if err:
            with open(err, "a") as f:
                f.write(header)
                f.write(stderr.decode("utf-8", "backslashreplace"))
    finally:
        if lock:
            lock.release()
