###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Entrypoint module: defines the CLI.
"""


import json
import logging
from pathlib import Path
from pydantic import BaseModel, field_validator
import sys
import typer
from typing import Optional
from .steps import CVMFSRepository, trace_ops, build_ops, test_ops, deploy_ops


# ------------------------------------------------------------------------------


class Operation(BaseModel):
    """Operation configuration"""

    # Command output
    out: Optional[str] = None
    # Command error
    err: Optional[str] = None


app = typer.Typer()


# ------------------------------------------------------------------------------


class Trace(Operation):
    """Trace configuration"""

    # Directory of applications structured as <apps_dir>/<command>/command.sh
    apps_dir_path: str
    # List of CVMFS repositories
    repositories: list[str]
    # Number of operations to be done in parallel
    number_cores: int = 1
    # Container to be used for tracing
    container: Optional[str] = None
    # File that will contain the list of the paths involved in the executions
    output: str

    @field_validator("apps_dir_path")
    @classmethod
    def apps_dir_valid(cls, v: str):
        """Check whether apps_dir exists and has a correct structure such as:

        <apps_dir>/<command>/command.sh
        """
        apps_dir_path = Path(v)
        if not apps_dir_path.exists():
            raise ValueError(f"{v} does not exist")

        # apps_dir should be valid
        if sum(1 for x in apps_dir_path.glob("*/command.sh")) == 0:
            raise ValueError(
                f"{v} does not contain any valid command:\n\t{v}/<command>/command.sh"
            )

        return v

    @field_validator("number_cores")
    @classmethod
    def number_cores_valid(cls, v: int):
        """Check whether number_cores exists and is a valid integer"""
        try:
            number_cores = int(v)
        except ValueError as e:
            raise ValueError(f"number-cores is not a valid integer: {v}") from e

        if number_cores < 0:
            raise ValueError(f"number-cores should not be negative: {number_cores}")
        return v


@app.command()
def trace(
    apps_dir: str = typer.Argument(
        ...,
        help="Directory of applications structured as <apps_dir>/<command>/command.sh",
    ),
    repositories: list[str] = typer.Argument(..., help="CVMFS repositories to trace"),
    number_cores: int = typer.Option(
        1, help="Number of operations to be done in parallel"
    ),
    container: Optional[str] = typer.Option(
        None, help="Container to be used for tracing"
    ),
    output: str = typer.Argument(
        ...,
        help="File that will contain the list of the paths involved in the executions",
    ),
    stdout: Optional[str] = typer.Option(
        None, help="Command output file (default: stdout)"
    ),
    stderr: Optional[str] = typer.Option(
        None, help="Command error file (default: stderr)"
    ),
    debug: bool = typer.Option(False, help="Enable debug logging"),
):
    """Trace applications of interest"""
    # Set the logging level
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    trace = Trace(
        repositories=repositories,
        apps_dir_path=apps_dir,
        number_cores=number_cores,
        container=container,
        output=output,
        out=stdout,
        err=stderr,
    )

    result = trace_ops(
        apps_dir=trace.apps_dir_path,
        repositories=trace.repositories,
        container=trace.container,
        path_list=trace.output,
        number_cores=trace.number_cores,
        out=trace.out,
        err=trace.err,
    )
    if not result:
        logging.error("Trace operation failed")
        sys.exit(1)


# ------------------------------------------------------------------------------


class Build(Operation):
    """Build configuration"""

    # List of CVMFS repositories
    repositories: dict[str, CVMFSRepository]
    # List of path mounts involved in the build process.
    path_list: list[str]
    # SubCVMFS path (the output)
    subset_path: str

    @field_validator("path_list")
    @classmethod
    def path_list_valid(cls, v: list[str]):
        for path_list_file in v:
            path_list_path = Path(path_list_file)
            if not path_list_path.exists():
                raise ValueError(f"{path_list_file} does not exist")

        return v


@app.command()
def build(
    repositories_config: str = typer.Argument(
        ..., help="CVMFS repositories configuration path"
    ),
    path_list: list[str] = typer.Argument(
        ..., help="List of files to be included in the subset"
    ),
    subset_path: str = typer.Argument(..., help="SubCVMFS directory path"),
    stdout: Optional[str] = typer.Option(
        None, help="Command output file (default: stdout)"
    ),
    stderr: Optional[str] = typer.Option(
        None, help="Command error file (default: stderr)"
    ),
    debug: bool = typer.Option(False, help="Enable debug logging"),
):
    """Build a subset of CVMFS based on lists of files"""
    # Set the logging level
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    # Open the repo config
    with open(repositories_config, "r") as f:
        repositories_content = json.load(f)

    # Validate the repo config
    cvmfs_repositories = {}
    for repo_name, repo_content in repositories_content.items():
        cvmfs_repositories[repo_name] = CVMFSRepository.model_validate(repo_content)

    build = Build(
        repositories=cvmfs_repositories,
        path_list=path_list,
        subset_path=subset_path,
        out=stdout,
        err=stderr,
    )

    result = build_ops(
        path_list=build.path_list,
        repositories=build.repositories,
        subset_path=build.subset_path,
        out=build.out,
        err=build.err,
    )
    if not result:
        logging.error("Build operation failed")
        sys.exit(1)


# ------------------------------------------------------------------------------


class Test(Operation):
    """Test configuration"""

    # Directory of applications structured as <apps_dir>/<command>/command.sh
    apps_dir_path: str
    # Number of operations to be done in parallel
    number_cores: int = 1
    # Path to the subset to be tested
    subset_path: str
    # Container to be used for testing
    container: Optional[str] = None

    @field_validator("apps_dir_path")
    @classmethod
    def apps_dir_valid(cls, v: str):
        apps_dir_path = Path(v)
        if not apps_dir_path.exists():
            raise ValueError(f"{v} does not exist")

        if sum(1 for x in apps_dir_path.glob("*/command.sh")) == 0:
            raise ValueError(
                f"{v} does not contain any valid command:\n\t{v}/<command>/command.sh"
            )
        return v

    @field_validator("number_cores")
    @classmethod
    def number_cores_valid(cls, v: int):
        try:
            number_cores = int(v)
        except ValueError as e:
            raise ValueError(f"number-cores is not a valid integer: {v}") from e

        if number_cores < 0:
            raise ValueError(f"number-cores should not be negative: {number_cores}")
        return v

    @field_validator("subset_path")
    @classmethod
    def subset_path_valid(cls, v: str):
        subset_path = Path(v)
        if not subset_path.exists():
            raise ValueError(f"{v} does not exist")
        return v


@app.command()
def test(
    apps_dir: str = typer.Argument(
        ...,
        help="Directory of applications structured as <apps_dir>/<command>/command.sh",
    ),
    subset_path: str = typer.Argument(..., help="Path to the subset to be tested"),
    number_cores: int = typer.Option(
        1, help="Number of operations to be done in parallel"
    ),
    container: Optional[str] = typer.Option(
        None, help="Container to be used for testing"
    ),
    stdout: Optional[str] = typer.Option(
        None, help="Command output file (default: stdout)"
    ),
    stderr: Optional[str] = typer.Option(
        None, help="Command error file (default: stderr)"
    ),
    debug: bool = typer.Option(False, help="Enable debug logging"),
):
    """Test applications of interest against a given subset"""
    # Set the logging level
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    test = Test(
        apps_dir_path=apps_dir,
        subset_path=subset_path,
        number_cores=number_cores,
        container=container,
        out=stdout,
        err=stderr,
    )

    result = test_ops(
        apps_dir=test.apps_dir_path,
        subset_path=test.subset_path,
        number_cores=test.number_cores,
        container=test.container,
        out=test.out,
        err=test.err,
    )
    if not result:
        logging.error("Test operation failed")
        sys.exit(1)


# ------------------------------------------------------------------------------


class Deploy(Operation):
    """Deploy configuration"""

    # Path to the subset to be deployed
    subset_path: str
    # Remote location where the subset will be deployed
    remote_location: str

    @field_validator("subset_path")
    @classmethod
    def subset_path_valid(cls, v: str):
        subset_path = Path(v)
        if not subset_path.exists():
            raise ValueError(f"{v} does not exist")
        return v


@app.command()
def deploy(
    subset_path: str = typer.Argument(..., help="Path to the subset to be deployed"),
    remote_location: str = typer.Argument(
        ..., help="Remote location where the subset will be deployed"
    ),
    stdout: Optional[str] = typer.Option(
        None, help="Command output file (default: stdout)"
    ),
    stderr: Optional[str] = typer.Option(
        None, help="Command error file (default: stderr)"
    ),
    debug: bool = typer.Option(False, help="Enable debug logging"),
):
    """Deploy subset of CVMFS on a remote machine"""
    # Set the logging level
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    deploy = Deploy(
        subset_path=subset_path, remote_location=remote_location, out=stdout, err=stderr
    )

    result = deploy_ops(
        subset_path=deploy.subset_path,
        remote_location=deploy.remote_location,
        out=deploy.out,
        err=deploy.err,
    )
    if not result:
        logging.error("Deploy operation failed")
        sys.exit(1)
