# SubCVMFS-builder

## Purpose

`SubCVMFS-builder` aims to help scientific communities to build a subset of CVMFS containing the minimal requirements to execute applications of interests and deploy it on a remote computing infrastructure.
The tool is mainly used on supercomputers having no outbound connectivity.

## Installation

- Before starting, you need to have at least `CVMFS` and `CVMFS-shrinkwrap` installed on the machine.

```
pip install subcvmfs-builder
```

Or:

```bash
git clone https://gitlab.cern.ch/alboyer/subcvmfs-builder.git
cd subcvmfs-builder

mamba env create -f environment.yaml
conda activate subcvmfs-builder

pip install -e .
```

## Usage

- Get the help:

```bash
subcvmfs --help
```

- Trace: run applications in a singularity container and return the list of dependencies related to CVMFS.

  ```bash
  subcvmfs trace --help
  ```

  *Trace is generally used when one has no idea about the dependencies of the applications to include in the subset of CVMFS*.

- Build: with a list of dependencies, build a subset of CVMFS using the `cvmfs-shrinkwrap` tool.

  ```bash
  subcvmfs build --help
  ```

  *Build is a required step to build a subset of CVMFS*.

- Test: execute application using a subset of dependencies instead of CVMFS.

  ```bash
  subcvmfs test --help
  ```

  *Test is generally used before deploying a subset of CVMFS. One may want to make sure the subset is valid against a certain set of applications*.

- Deploy: Deploy the subset of CVMFS - within a container if needed - on the remote computing infrastructure.

  ```bash
  subcvmfs deploy --help
  ```

  *Deploy requires one to have access to the remote computing infrastructure*.

## Configuration

- Minimal configuration possible:

```json
{
	"cvmfs_extensions":
	{
		"<repository_name>":
		{
			"url": "<repository_url>",
			"public_key": "<repository_pubkey>",
		},
	}
}
```

## References

- Further details about the context can be found in: https://arxiv.org/abs/2303.16675.
- A generic example of a `subcvmfs-builder` pipeline can be found in: https://gitlab.cern.ch/alboyer/subcvmfs-builder-pipeline
